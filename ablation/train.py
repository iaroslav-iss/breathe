import numpy as np
import reader
import model_io as mio


# todo: autoatically assign the labels
encoding = {"normal":0, "extrahls":1, "murmur":2, "artifact":3}
val_fraction = 0.3

# todo: determine the best size of subsequence
slice_size = 160

if __name__ == "__main__":

    X, Y = reader.read_dataset("dataset/set_a", {"murmur", "extrahls", "normal", "artifact"})

    # split into validation and testing datasets
    Xv, Xt, Yv, Yt = [], [], [], []

    for x, y in zip(X, Y):
        if np.random.rand() > val_fraction:
            Xt.append(x)
            Yt.append(y)
        else:
            Xv.append(x)
            Yv.append(y)


    # todo: normalization
    def generate_batch(X, Y, encoding, n):
        Xb, Yb = [], []
        i = 0
        while i < n:

            idx = np.random.randint(len(X))
            x = X[idx]
            y = Y[idx]
            max_offset = len(x) - slice_size

            if max_offset < 0:
                continue

            slice = np.random.randint(max_offset)
            subset = x[slice:(slice+slice_size)]
            subset = subset[np.newaxis, :, :] # first channel should be the color channel, even if you have a single channel
            Xb.append(subset/255.0)
            Yb.append(encoding[y])
            i += 1

        Xb = np.array(Xb).astype("float32")
        Yb = np.array(Yb).astype("int32")

        return Xb, Yb


    from keras.layers import Input, Convolution2D, MaxPooling2D, Flatten, Dense
    from keras.layers.advanced_activations import LeakyReLU
    from keras.models import Model

    # this returns a tensor
    inputs = Input(shape=(1, slice_size,513))
    x = inputs

    for i in range(6):
        x = Convolution2D(32, 3,3, border_mode="same")(x)
        x = LeakyReLU()(x)
        x = MaxPooling2D()(x)

    x = Flatten()(x)

    for i in range(3):
        x = Dense(64)(x)
        x = LeakyReLU()(x)

    predictions = Dense(len(encoding.keys()), activation='softmax')(x)

    # this creates a model that includes
    # the Input layer and three Dense layers
    model = Model(input=inputs, output=predictions)
    model.compile(optimizer='rmsprop',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    mio.save_keras_model(model, "model")

    import time

    best_acc = -np.inf


    while True:

        st = time.time()

        for i in range(10):
            x, y = generate_batch(X, Y, encoding, 128)
            model.train_on_batch(x, y)


        L, A = [], []
        for i in range(10):
            x, y = generate_batch(Xv, Yv, encoding, 128)
            loss, acc = model.test_on_batch(x, y)
            L.append(loss)
            A.append(acc)


        if acc > best_acc:
            mio.save_keras_model(model, "model")


        print 'Accuracy', np.mean(A), "Loss", np.mean(L), "time", str(time.time()-st)

