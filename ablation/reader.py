#!/usr/bin/env python
#coding: utf-8
""" This work is licensed under a Creative Commons Attribution 3.0 Unported License.
    Frank Zalkow, 2012-2013 """

from matplotlib import pyplot as plt
import os

from spectrum import wav_to_img, read_wav, plotstft


def read_dataset(dataset_folder, labels):
    X, Y = [], []
    files = os.listdir(dataset_folder)


    for file in files:
        try:
            # check if file name contains a known label
            known_label = None
            for label in labels:
                if label in file:
                    known_label = label
                    break

            # if not contiune
            if known_label is None:
                print "Skipping " + file + " as it does not contain any known label in its name"
                continue

            samplerate, samples = read_wav(os.path.join(dataset_folder, file))
            img = wav_to_img(samples, samplerate)

            X.append(img)
            Y.append(known_label)

            #break

        except BaseException as ex:
            # something is wrong - eg file is corrupt. Skipping.
            print "Exception while reading ", file
            print ex
            continue



    return X, Y


if __name__ == "__main__":

    path = "dataset/set_a/"
    files = os.listdir(path)

    plt.figure(figsize=(15, 7.5))
    plt.ion()

    for f in files:
        print f
        name = os.path.join(path, f)
        plotstft(name, plotpath=name + ".jpg")
