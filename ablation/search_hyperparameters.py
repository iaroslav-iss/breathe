import json
import train
from skopt import space
import numpy as np
from skopt import Optimizer
from skopt.learning import GaussianProcessRegressor, RandomForestRegressor, ExtraTreesRegressor
import random

parameters = {
    "n_neurons":space.Integer(4, 64),
    "n_layers":space.Integer(1, 4),
    "alpha":space.Real(-3.0, -1.0),
    "beta_1":space.Real(0.01, 0.99),
    "beta_2":space.Real(0.01, 0.99),
    "batch_size":space.Integer(8, 128),
}

def example_function_to_be_optimized(
    n_neurons,
    n_layers,
    alpha,
    beta_1,
    beta_2,
    batch_size):
    return n_neurons / 64 + alpha/3.0 + batch_size / 128

estimator = GaussianProcessRegressor()
solver = Optimizer([v for k,v in parameters.iteritems()], estimator, acq_optimizer="sampling")

# random tag
rnd_tag = str(random.randint(0, 2**31))

max_iter = 256
best_y = np.inf

for i in range(max_iter):
    p = solver.ask()

    point = {k: v for k, v in zip(parameters.keys(), p)}
    try:
        v = example_function_to_be_optimized(**point)
    except BaseException as ex:
        v = 1.0
        print ex

    solver.tell(p, v)

    if best_y > v:
        best_y = v
        best_x = point
        print("Improved! " + str(best_y))

        with open(rnd_tag+".json", "w") as f:
            json.dump({'point': best_x, 'objective': best_y}, f)

    print("Eval. #" + str(i))
