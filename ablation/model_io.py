from keras.models import model_from_json
import os
import json

MODEL_WEIGHTS_FILE = "model.weights"
MODEL_TOPOLOGY_FILE = "model.topology"

def save_keras_model(m, foldr):
    if not os.path.exists(foldr):
        os.mkdir(foldr)
    js = m.to_json()
    m.save_weights(os.path.join(foldr, MODEL_WEIGHTS_FILE))
    json.dump(js, open(os.path.join(foldr, MODEL_TOPOLOGY_FILE), 'w'))

def load_keras_model(foldr):
    if not os.path.exists(foldr):
        raise BaseException("The folder with model does not exist.")

    js = json.load(open(os.path.join(foldr, MODEL_TOPOLOGY_FILE)))
    model = model_from_json(js)
    model.load_weights(os.path.join(foldr, MODEL_WEIGHTS_FILE))

    return model