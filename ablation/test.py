import pyaudio
import wave
import urllib
from spectrum import wav_to_img
import cv2
import matplotlib.pyplot as plt
import spectrum as spc
import model_io as mio
import numpy as np
import train as tds

# load the model
model = mio.load_keras_model("model")

CHUNK = 128

audio_stream = urllib.urlopen('http://192.168.1.48:8080/audio.wav')
wf = wave.open(audio_stream)

# instantiate PyAudio (1)
p = pyaudio.PyAudio()

# open stream (2)
stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)

spec_data = []

# read data
data = wf.readframes(CHUNK)


spec_data = data
FR = 44100/2
IR = FR*12

# play stream (3)
while len(data) > 0:
    stream.write(data)

    if len(spec_data) > IR:
        signal = np.fromstring(spec_data, dtype='int16')
        spec_data = spec_data[FR:]

        fft_img = wav_to_img(signal)
        img = fft_img - 100.0

        nn_input = fft_img[-tds.slice_size:, :]
        nn_input = nn_input[np.newaxis, np.newaxis, :, :]

        otp = model.predict(nn_input)[0]
        otp = np.argmax(otp)

        for k,v in tds.encoding.iteritems():
            if v == otp:
                print "diagnosis", k

        img = np.transpose(img)
        img = np.stack([img*2.0, img*0.0, img*0.0], axis=2)
        img = img.astype('uint8')
        img = cv2.resize(img, (img.shape[0]*3,img.shape[1]*2))
        cv2.imshow('Spectrogram',img)
        if cv2.waitKey(1) ==27:
            exit(0)


    data = wf.readframes(CHUNK)
    spec_data += data


# stop stream (4)
stream.stop_stream()
stream.close()

# close PyAudio (5)
p.terminate()